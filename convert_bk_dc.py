from argparse import ArgumentParser
from re import search

arg_parser = ArgumentParser()

arg_parser.add_argument("-i", "--input", required=True, help="the bk file to convert to dc")
arg_parser.add_argument("-o", "--output", required=True, help="where to output the dc converted file")
cmd_args = vars(arg_parser.parse_args())

input_path = cmd_args['input']
input_file = open(input_path, "r")
output_path = cmd_args['output']
output_file = open(output_path, "w")

for line in input_file:
	brick_list = search("('\d{1,10}'(, )?)+", line).group()
	brick_list = brick_list.replace("'", "")
	brick_list = brick_list.replace(" ", "")
	output_file.write(brick_list)
	output_file.write("\n")

input_file.close()
output_file.close() 

